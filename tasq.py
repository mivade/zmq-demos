import functools
import inspect
import time
from uuid import uuid4
from multiprocessing import Process, Event, Manager
import zmq

registered_tasks = dict()


def task(func):
    """Decorator to register a function as a task."""
    if func.__name__ not in registered_tasks:
        sig = inspect.signature(func)
        registered_tasks[func.__name__] = {
            'func': func,
            'params': list(sig.parameters.keys())
        }

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


@task
def slow_add(a, b):
    time.sleep(0.5)
    return a + b


class StoppableProcess(Process):
    _done = Event()

    @property
    def done(self):
        return self._done.is_set()

    @done.setter
    def done(self, value):
        if value:
            self._done.set()
        else:
            self._done.clear()

    def stop(self):
        self.done = True


class Broker(StoppableProcess):
    """Process to push jobs to workers."""
    address_in = 'tcp://*:9190'
    address_out = 'tcp://*:9191'

    def run(self):
        ctx = zmq.Context()

        # Socket for accepting jobs
        acceptor = ctx.socket(zmq.REP)
        acceptor.bind(self.address_in)

        # Socket for forwarding jobs to workers
        ventilator = ctx.socket(zmq.PUSH)
        ventilator.bind(self.address_out)

        while not self.done:
            if acceptor.poll(timeout=1000):
                try:
                    msg = acceptor.recv_json()
                    uid = str(uuid4())
                    msg['uid'] = uid
                    ventilator.send_json(msg)
                    acceptor.send_json({'uid': uid})
                except Exception as e:
                    print(e)


class Worker(StoppableProcess):
    def run(self):
        ctx = zmq.Context()

        jobsock = ctx.socket(zmq.PULL)
        jobsock.connect('tcp://127.0.0.1:9191')

        backend = ctx.socket(zmq.PUSH)
        backend.connect('tcp://127.0.0.1:9192')

        while not self.done:
            if jobsock.poll(timeout=1000):
                try:
                    msg = jobsock.recv_json()
                    fname = msg['func']
                    func = registered_tasks[fname]['func']
                    params = msg['params']
                    result = func(**params)
                    print("Worker returned", result)
                    backend.send_json({'uid': msg['uid'], 'result': result})
                except Exception as e:
                    print(e)


class Backend(Process):
    """Results storage base class."""
    def __init__(self, address='tcp://*:9192'):
        super(Backend, self).__init__()
        self._done = Event()
        self.address = address

    def store(self, uid, result):
        """Store the results of a task.

        :param str uid: Unique ID.
        :param result: Results of the task.

        """
        raise NotImplementedError

    def get(self, uid):
        """Retrieve results with the supplied unique ID.

        :param str uid:

        """
        raise NotImplementedError

    def remove(self, uid):
        """Remove the result with the given unique ID.

        :param str uid:

        """
        raise NotImplementedError

    def clear(self):
        """Remove *all* results from storage."""
        raise NotImplementedError

    def stop(self):
        """Stop the backend service."""
        self._done.set()

    def run(self):
        """Run the backend service."""
        print("running")
        ctx = zmq.Context()
        sock = ctx.socket(zmq.PULL)
        sock.bind(self.address)

        while not self._done.is_set():
            if sock.poll(timeout=1000):
                try:
                    results = sock.recv_json()
                    print(results)
                    self.store(results['uid'], results['result'])
                except Exception as e:
                    print(e)


class MemoryBackend(Backend):
    """A simple shared memory backend.

    :param memstore: A shared dict created with a :class:`multiprocessing.Manager`.

    """
    def __init__(self, memstore, address='tcp://*:9192'):
        super(MemoryBackend, self).__init__(address)
        self.memory = memstore

    def store(self, uid, result):
        self.memory[uid] = result

    def get(self, uid):
        return self.memory[uid]

    def remove(self, uid):
        return self.memory.pop(uid)

    def clear(self):
        self.memory.clear()


if __name__ == "__main__":
    import random

    with Manager() as mgr:
        storage = mgr.dict()
        backend = MemoryBackend(storage)
        backend.start()

        broker = Broker()
        broker.start()

        worker = Worker()
        worker.start()

        ctx = zmq.Context()
        sock = ctx.socket(zmq.REQ)
        sock.connect('tcp://127.0.0.1:9190')
        for _ in range(10):
            sock.send_json({
                'func': 'slow_add',
                'params': {
                    'a': random.randint(0, 10),
                    'b': random.randint(0, 10)
                }
            })
            print("Submitted job id", sock.recv_json()['uid'])

        time.sleep(6)
        worker.stop()
        broker.stop()

        for key in storage.keys():
            print(key, backend.get(key))

        backend.stop()
