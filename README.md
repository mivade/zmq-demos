# ZMQ Demos

[![build status](https://gitlab.com/mivade/zmq-demos/badges/master/build.svg)](https://gitlab.com/mivade/zmq-demos/commits/master)

Simple examples of using [ZeroMQ][] in Python and C++.

## Requirements

Python demos require that [PyZMQ][] is installed. C++ demos use [cppzmq][] and
are most easily built using [CMake][]. If you are using [conda][], requirements
can be installed with:

```
conda install cmake pyzmq
conda install -c conda-forge cppzmq
```

## Building C++ demos

```
mkdir build
cd build
cmake ..
cmake --build .
```

[ZeroMQ]: http://zeromq.org/
[PyZMQ]: https://pyzmq.readthedocs.io/en/latest/index.html
[cppzmq]: https://github.com/zeromq/cppzmq
[CMake]: https://cmake.org/
[conda]: https://conda.io/docs/
