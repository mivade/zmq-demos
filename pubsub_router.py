"""Demo of using a ZMQ proxy to forward published messages from multiple
services to multiple subscribers.

"""

import time
import random
from threading import Thread
from multiprocessing import Process, Event
import zmq


class MessageRouter(Process):
    """Router process. This is implemented as a separate process rather
    than as a thread just for the sake of demonstrating that it works.

    """
    INCOMING_PORT = 9000
    OUTGOING_PORT = 9001

    _stop_signal = Event()

    def stop(self):
        self._stop_signal.set()

    def run(self):
        ctx = zmq.Context()

        # The socket on which to receive messages
        frontend = ctx.socket(zmq.SUB)
        frontend.setsockopt(zmq.SUBSCRIBE, b'')
        frontend.bind('tcp://127.0.0.1:{:d}'.format(self.INCOMING_PORT))

        # The socket on which to send messages
        backend = ctx.socket(zmq.PUB)
        backend.bind('tcp://127.0.0.1:{:d}'.format(self.OUTGOING_PORT))

        # The proxy device. This blocks indefinitely so is placed in a
        # thread which allows us to monitor for a stop signal.
        def proxy():
            try:
                zmq.proxy(frontend, backend)
            except zmq.ContextTerminated:
                print("Router exiting.")
                frontend.close()
                backend.close()

        Thread(target=proxy).start()
        self._stop_signal.wait()
        print("Router terminating.")


class Publisher(Process):
    """The originator of some type of message.

    :param bytes channel: The channel to publish to.

    """
    def __init__(self, channel, *args, **kwargs):
        self.channel = channel
        super(Publisher, self).__init__(*args, **kwargs)

    def run(self):
        ctx = zmq.Context()
        sock = ctx.socket(zmq.PUB)
        sock.connect('tcp://127.0.0.1:{:d}'.format(MessageRouter.INCOMING_PORT))

        time.sleep(1)
        for n in range(10):
            sock.send_multipart([self.channel, b"Hello world"])
            time.sleep(random.uniform(1, 2))

        print("Publisher {:s} terminating".format(self.name))


class Subscriber(Thread):
    """Receiver of messages.

    :param list channels: List of channels to subscribe to

    """
    def __init__(self, channels, *args, **kwargs):
        self.channels = channels
        super(Subscriber, self).__init__(*args, **kwargs)

    def run(self):
        ctx = zmq.Context()
        sock = ctx.socket(zmq.SUB)
        for channel in self.channels:
            sock.setsockopt(zmq.SUBSCRIBE, channel)
        sock.connect('tcp://127.0.0.1:{:d}'.format(MessageRouter.OUTGOING_PORT))

        t0 = time.time()
        while time.time() - t0 <= 10:
            channel, message = sock.recv_multipart()
            print("{:s} received from {!s}: {!s}".format(self.name, channel, message))

        print("Subscriber '{:s}' terminating.".format(self.name))



if __name__ == "__main__":
    router = MessageRouter()
    router.start()

    channels = [b'one', b'two', b'three']
    pubs = [Publisher(ch, name=ch.decode()) for ch in channels]
    subs = [Subscriber(channels, name='Thread {:d}'.format(n)) for n in range(2)]

    for pub in pubs:
        pub.start()
    for sub in subs:
        sub.start()

    for pub in pubs:
        pub.join()

    router.stop()
