"""Message queue implemented with two sockets:

1. ``zmq.PUB`` to publish messages to subscribers.
2. ``zmq.PULL`` to receive new messages to publish.

"""

import sys
import json
import asyncio

import zmq
from zmq.asyncio import ZMQEventLoop, Context

PORT = 9998


class MessageQueue:
    _protocol = "tcp"
    _address = "127.0.0.1"

    def __init__(self):
        self.ctx = Context()
        self.queue = asyncio.Queue()

    @classmethod
    def address(cls, port):
        return "{:s}://{:s}:{:d}".format(cls._protocol, cls._address, port)

    async def listen_loop(self):
        """Listen for incoming messages."""
        incoming = self.ctx.socket(zmq.PULL)
        incoming.bind(self.address(PORT + 1))

        while True:
            msg = await incoming.recv_multipart()
            print("received: {}".format(msg))
            await self.queue.put(msg)

    async def publish_loop(self):
        """Consume messages from the queue and publish to subscribers."""
        outgoing = self.ctx.socket(zmq.PUB)
        outgoing.bind(self.address(PORT))

        while True:
            msg = await self.queue.get()
            await outgoing.send_multipart(msg)
            await asyncio.sleep(0.1)  # artificial throttling

    async def start(self):
        await asyncio.gather(self.listen_loop(), self.publish_loop())


def client(channel="default"):
    ctx = zmq.Context()

    receiver = ctx.socket(zmq.SUB)
    receiver.setsockopt(zmq.SUBSCRIBE, b"")
    receiver.connect(MessageQueue.address(PORT))

    sender = ctx.socket(zmq.PUSH)
    sender.connect(MessageQueue.address(PORT + 1))

    poller = zmq.Poller()
    poller.register(receiver, zmq.POLLIN)

    print("Starting client...")

    for n in range(100):
        if receiver in dict(poller.poll(timeout=500)):
            msg = receiver.recv_multipart()
            print("Received: {}".format(msg))

        outgoing = json.dumps({"n": n, "channel": channel})
        print("Sending message: {:s}".format(outgoing))
        sender.send_multipart([outgoing.encode()])


if __name__ == "__main__":
    if sys.argv[-1] == "client":
        client()
    else:
        loop = ZMQEventLoop()
        asyncio.set_event_loop(loop)

        q = MessageQueue()
        loop.run_until_complete(q.start())
