import time
import json
from threading import Thread

import zmq
from zmq.error import ZMQError
from zmq.eventloop.ioloop import ZMQIOLoop
from zmq.eventloop.zmqstream import ZMQStream


class ZmqSocketConnection(Thread):
    """ZMQ-based socket connection.

    :param str address: ZMQ address string
    :param zmq.Context ctx:
    :param bool server: Act as a "server" (bind instead of connect)

    """
    def __init__(self, address, ctx=None, server=False):
        super(ZmqSocketConnection, self).__init__()

        self._loop = ZMQIOLoop()

        self.ctx = ctx or zmq.Context()
        self.address = address

        self.sock = self.ctx.socket(zmq.PAIR)

        self.stream = ZMQStream(self.sock, self._loop)
        self.stream.on_recv(self.receive_callback)

        self._server = server

    def __enter__(self):
        return self

    def __exit__(self, t, v, tb):
        self.close()

    def close(self):
        """Close the socket connection and exit."""
        self._loop.close()
        self.sock.close()

    def receive_callback(self, msg):
        """Callback to process incoming messages."""
        msg = ''.join([m.decode() for m in msg])
        try:
            data = json.loads(msg)
        except json.JSONDecodeError:
            print("malformed JSON: %s" % msg)
            return
        print("received:", data)
        self.stream.send_json(dict(timestamp=time.time(), original=data))

    def run(self):
        """Main thread for handling message passing."""
        # Bind or connect
        if self._server:
            self.sock.bind(self.address)
        else:
            self.sock.connect(self.address)

        print("Starting IO loop...")
        self._loop.start()


def server(addr):
    with ZmqSocketConnection(addr, server=True) as server:
        server.start()
        try:
            server.join()
        except KeyboardInterrupt:
            pass


def client(addr):
    socket = zmq.Context().socket(zmq.PAIR)
    socket.connect(addr)
    for i in range(10):
        msg = dict(timestamp=time.time(), body="hi", idx=i)
        print("-->", msg)
        socket.send_json(msg)
        print("<--", socket.recv_json())
        time.sleep(0.2)

    # with ZmqSocketConnection(addr, server=False) as client:
    #     for i in range(10):
    #         msg = dict(timestamp=time.time(), body="hi")
    #         print("sending")
    #         client.send_json(msg)
    #         print(client.recv_json())
    #         time.sleep(0.2)


if __name__ == "__main__":
    import sys

    host = "127.0.0.1"
    port = 8998
    addr = "tcp://{:s}:{:d}".format(host, port)

    try:
        if sys.argv[1] == "client":
            client(addr)
    except IndexError:
        server(addr)
