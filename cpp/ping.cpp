#include <iostream>
#include <zmq_addon.hpp>


int main()
{
    zmq::context_t ctx(1);
    zmq::socket_t socket(ctx, ZMQ_REP);
    socket.bind("tcp://*:9080");

    while (true)
    {
        zmq::multipart_t msg(socket);
        std::cout << "Got " << msg.str() << "\n";

        zmq::multipart_t outgoing;
        outgoing.addstr("Pong");
        outgoing.send(socket);
    }

    return 0;
}
