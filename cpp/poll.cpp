#include <iostream>
#include <vector>
#include <chrono>
#include <string>
#include <zmq_addon.hpp>

using socket_vec = std::vector<zmq::socket_t *>;


struct PollResult
{
    /// List of sockets that are currently readable.
    socket_vec readable;

    /// List of sockets that are currently writable.
    socket_vec writable;
};


class Poller
{
private:
    std::vector<zmq::pollitem_t> items;
    std::vector<zmq::socket_t *> sockets;

public:

    Poller() : items(), sockets() {}

    ~Poller()
    {
        items.clear();
        sockets.clear();
    }

    // PyZMQ would call this method register, but register is a reserved keyword in C++.
    void add(zmq::socket_t &socket, const short flags=ZMQ_POLLIN)
    {
        zmq::pollitem_t item = { socket, 0, flags, 0 };
        items.push_back(item);
        sockets.push_back(&socket);
    }

    inline auto poll(std::chrono::milliseconds timeout) -> PollResult
    {
        auto res = zmq::poll(items, timeout);
        auto socks = PollResult();

        if (res != 0)
        {
            for (int i = 0; i < items.size(); i++)
            {
                zmq::pollitem_t item = items[i];
                if (item.revents & ZMQ_POLLIN)
                    socks.readable.push_back(sockets[i]);
                if (item.revents & ZMQ_POLLOUT)
                    socks.writable.push_back(sockets[i]);
            }
        }

        return socks;
    }
};


int main()
{
    auto ctx = zmq::context_t(1);
    auto socket = zmq::socket_t(ctx, zmq::socket_type::rep);
    socket.bind("tcp://127.0.0.1:9080");
    Poller poller;
    poller.add(socket);

    for (int i = 0; i < 30; ++i)
    {
        auto socks = poller.poll(std::chrono::milliseconds(1000));
        for (auto &sock: socks.readable)
        {
            // std::cout << (sock == &socket) << std::endl;
            zmq::multipart_t msg;
            msg.recv(*sock);
            std::cout << msg.str() << std::endl;
            msg.send(*sock);
        }
    }

    return 0;
}
