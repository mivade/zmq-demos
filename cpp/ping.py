import zmq

ctx = zmq.Context()
sock = ctx.socket(zmq.REQ)
sock.connect('tcp://127.0.0.1:9080')

sock.send_multipart([b"Pinging!"])
print(sock.recv_multipart())
