import zmq

ctx = zmq.Context()
sock = ctx.socket(zmq.SUB)
sock.connect('tcp://127.0.0.1:9080')
sock.setsockopt(zmq.SUBSCRIBE, b'world')

while True:
    print(sock.recv_multipart())
