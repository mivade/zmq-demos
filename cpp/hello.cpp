/*
  If cppzmq is installed with conda, compile with something like:

      g++ -std=c++11 -lzmq -I$HOME/miniconda3/include -L$HOME/miniconda3/lib hello.cpp
*/

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <zmq_addon.hpp>


/// Create a multipart message for publishing on a specific topic.
inline auto make_message(const std::string &topic, const std::string &data) -> zmq::multipart_t
{
    zmq::multipart_t msg;
    msg.addstr(topic);
    msg.addstr(data);
    return msg;
}


int main()
{
    zmq::context_t ctx(1);
    zmq::socket_t socket(ctx, ZMQ_PUB);
    socket.bind("tcp://*:9080");

    while (true)
    {
        auto hello = make_message("hello", "hello");
        auto world = make_message("world", "world");
        std::cout << hello.str() << "\n" << world.str() << "\n";
        hello.send(socket);
        world.send(socket);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}
